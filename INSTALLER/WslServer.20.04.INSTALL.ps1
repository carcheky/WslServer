if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
sleep 1




echo "==> instalando nuevo wsl"
wsl --import "WslServer.20.04" C:\wsl\server\distro\WslServer C:\wsl\INSTALLER\WslServer.tar.gz
wsl --list -v







echo seleccionando usuario...

Set-ExecutionPolicy Unrestricted

sleep 1

Get-ItemProperty Registry::HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Lxss\*\ DistributionName | Where-Object -Property DistributionName -eq "WslServer.20.04" | Set-ItemProperty -Name DefaultUid -Value ((wsl -d "WslServer.20.04" -u user -e id -u) | Out-String);

sleep 5
