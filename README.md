# WslServer

- Usando Git bash, Clona el repo en tu unidad C:\\wsl

    ````git
    git clone --recurse-submodules https://gitlab.com/carcheky/WslServer.git C:\\wsl
    ````

- Descarga el siguiente archivo y colócalo en ``C:\wsl\INSTALLER\WslServer.tar.gz``

    [Descargar WslServer.tar.gz](https://drive.google.com/drive/folders/1Uw5rjVzwM7xooR1Juyr_EHQdHPPZbEzP?usp=sharing)

- Doble clic ``C:\wsl\INSTALLER\INSTALL_SERVER.cmd``
